const Cart = require("../models/Cart");
const Product = require('../models/Product');
const cartController = require("../controllers/cartControllers");


// Get the current items in the cart
exports.getCartItems = (req, res) => {
  const cart = new Cart(req.session.cart);
  const cartItems = cart.getItems();
  res.render('cart', { cartItems });
  req.session.save((err) => {
    if (err) {
      console.log(err);
    }
  });
};

//Add to cart
exports.addToCart = async (req, res) => {
  const productId = req.body.productId;
  const qty = parseInt(req.body.qty);
  const cart = new Cart(req.session.cart);

  try {
    const product = await Product.findById(productId);
    if (!product) {
      return res.status(404).json({ message: 'Product not found' });
    }

    cart.add(product, productId, qty);
    req.session.cart = cart;
    res.status(200).json(cart);
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Internal server error' });
  }
};

// Change the quantity of a product in the cart
exports.changeQuantity = async (req, res) => {
  const productId = req.params.productId;
  const qty = parseInt(req.body.qty); // change this line to get qty from req.body
  const cart = new Cart(req.session.cart);

  try {
    const product = await Product.findById(productId);
    if (!product) {
      return res.status(404).json({ message: 'Product not found' });
    }

    cart.changeQuantity(product, productId, qty);
    req.session.cart = cart;
    res.status(200).json(cart);
  } catch (err) {
    console.error(err);
    res.status(500).json({ message: 'Internal server error' });
  }
};
