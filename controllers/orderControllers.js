const Order = require("../models/Order");
const Product = require("../models/Product");


//Creating an order
module.exports.createOrder = async (req, res) => {
  try {
    const { orders, userId } = req.body;

    // Get all products for the given order
    const productIds = orders.map((order) => order.productId);
    const products = await Product.find({ _id: { $in: productIds } });
    const productMap = products.reduce((map, product) => {
      map[product._id.toString()] = product;
      return map;
    }, {});

    // Create new orders and update products
    const newOrders = [];
    const updatedProducts = [];
    for (const order of orders) {
      const { productId, quantity } = order;
      const product = productMap[productId];
      if (!product) {
        console.error(`Product with ID ${productId} not found`);
        continue; // skip orders for products that were not found
      }
      const subTotal = product.price * quantity;
      const totalPrice = subTotal;
      const newOrder = new Order({
        product: productId,
        quantity,
        subTotal,
        totalPrice,
        user: userId,
      });
      newOrders.push(newOrder);
      updatedProducts.push({
        id: product._id,
        order: {
          orderId: newOrder._id,
          quantity: newOrder.quantity,
        },
      });
    }

    // Save new orders and update products in parallel
    const [savedOrders, savedProducts] = await Promise.all([
      Order.insertMany(newOrders),
      Product.bulkWrite(
        updatedProducts.map(({ id, order }) => ({
          updateOne: {
            filter: { _id: id },
            update: { $push: { orders: order } },
          },
        }))
      ),
    ]);

    // Return a success message and the array of saved orders
    return res.status(201).json({
      message: "Orders created successfully",
      orders: savedOrders,
    });
  } catch (err) {
    console.error(err);
    return res.status(500).json({ error: "Server error" });
  }
};

// Get all orders
module.exports.getAllOrders = () => {

  return Order.find({}).then(result=>{
    return result;
  });
};
