const Product = require("../models/Product");

//Add a product
/*module.exports.addProduct = (data) =>{

	if(data.isAdmin){

			let newProduct = new Product({

		name: data.product.name,
		description: data.product.description,
		price: data.product.price

	});

		return newProduct.save().then((product,error)=>{

		if (error){
			return false
		}else{
			return `Product successfully added!`
		}

	})

	}else{
		return false
	}

};*/


module.exports.addProduct = async (data) => {
  if (!data.isAdmin) {
    return false;
  }

  try {
    const newProduct = new Product({
      name: data.product.name,
      description: data.product.description,
      price: data.product.price,
    });
    const savedProduct = await newProduct.save();
    return true;
  } catch (error) {
    console.log(error);
    return false;
  }
};


//Retrieve all products

module.exports.getAllProducts = () => {

	return Product.find({}).then(result=>{
		return result;
	});
};


//Retrieve all ACTIVE products
module.exports.getAllActive = () => {

	return Product.find({isActive:true}).then(result=>{
		return result;
	});
};

//Retrieve by productId
module.exports.getProductById = (productId) => {

    return Product.findById(productId).then(result=>{
        if(result){
            return result;
        }else{
            throw new Error(`Product with ID ${productId} not found`);
        }
    });
};

// Update a product by ID
/*module.exports.updateProductById = (productId, updatedProduct) => {
    return Product.findByIdAndUpdate(productId, updatedProduct).then(result => {
        if(result) {
            return `Product with ID ${productId} updated successfully`;
        } else {
            throw new Error(`Product with ID ${productId} not found`);
        }
    });
};*/

module.exports.updateProduct = async(data, reqParams) => {

	if(data.isAdmin) {

		if(data.product) {
			if(data.product.image) {
			const uploadRes = await cloudinary.uploader.upload(data.product.image, {
				upload_preset: "win-food"
			});
			if(uploadRes) {
				const updatedProduct = {
					name: data.product.name,
					description: data.product.description,
					price: data.product.price,
					image: uploadRes
				};
				return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
					if(error) {
						return false;
					} else {
						return true;
					};
				});
			};
			} else {
			const updatedProduct = {
				name: data.product.name,
				description: data.product.description,
				price: data.product.price,
			}
				return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
					if(error) {
						return false;
					} else {
						return true;
					};
				});
			}
		};
	}
};



//Archive a product
module.exports.archiveProduct = (data, reqParams, reqBody) => {
  if (data.isAdmin) {
    let archiveProduct = {
      isActive: false
    };
    return Product.findByIdAndUpdate(reqParams.productId, archiveProduct)
      .then((product, error) => {
        if (error) {
          return false;
        } else {
          return true;
        }
      });
  } else {
    return false;
  }
};
//Unarchive a product

module.exports.unarchiveProduct = (data, reqParams, reqBody) => {
  if (data.isAdmin) {
    let unarchiveProduct = {
      isActive: true
    };
    return Product.findByIdAndUpdate(reqParams.productId, unarchiveProduct)
      .then((product, error) => {
        if (error) {
          return false;
        } else {
          return true;
        }
      });
  } else {
    return false;
  }
};