const User = require("../models/User");
const Product = require("../models/Product")
const Order = require("../models/Order")


const bcrypt = require("bcrypt");
const auth = require("../auth");

//Controller for user registration
module.exports.registerUser = (reqBody) =>{

	let newUser = new User({

		firstName : reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)

	})

	return newUser.save().then((user,error)=>{

		if(error){
			return false
		}else{
			return newUser
		}
	})	

}


//End of user registration


//Controller for user authentication
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result=>{

		if(result === null){

			return false;

		}else{

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect){

				return {access: auth.createAccessToken(result)}

			}else{

				return false;
			}


		}


	})
}

//End of user authentication


//Controller for check email
module.exports.checkEmailExists = (reqBody) => {

	//the result is sent back to the front end via the .then method found in the route file

	return User.find({email: reqBody.email}).then(result => {

		//the find method returns a record if a match is found
		if(result.length>0){
			return `User already exists!`;
		//no duplicate email found
		//user is not yet registered in the database	
		}else{
			return false
		}
	})

};

//End of check email

//Set user as admin

module.exports.setAsAdmin = (reqParams, reqBody) =>{

	let updatedAdminStatus = {

		isAdmin : true
	}
	return User.findByIdAndUpdate(reqParams.userId, updatedAdminStatus).then((
		user,error)=>{

		if(error){
			return false
		}else{
			return user;
		};

	});

};

//End of set user as admin

//Get user details by user ID
/*module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result=>{

		result.password = "";
		return result;
	})
}*/
module.exports.getProfile = (data) => {
return User.findById(data.userId).then((result) => {
result.password = '';
return result;
});
};


//purchase product
module.exports.purchase = async (req, res) => {
  const { productId } = req.params;
  const user = req.user;

  // Validate if the user is an admin
  if (user.isAdmin) {
    return res.status(403).send({ message: "Action Forbidden." });
  }

  try {
    // Find the product that the user wants to purchase
    const product = await Product.findById(productId);
    if (!product) {
      return res.status(404).send({ message: "Product not found." });
    }

    // Create a new order with the user ID and product ID
    const order = await Order.create({
      productId: productId,
      userId: user.id,
      quantity: 1,
    });

    // Save the new order
    await order.save();

    // Send a response to the client confirming the purchase
    return res.send({ message: "Thank you for your purchase!" });
  } catch (error) {
    console.log(error);
    return res.status(500).send({ message: "Internal server error." });
  }
};

// [UPDATE USER DATA: START]

module.exports.updateDetails = (reqParams, reqBody) => {

	let editProfile = {
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo
	};
	return User.findByIdAndUpdate(reqParams.userId, editProfile).then((user, error) => {
		if(error) {
			return false;
		} else {
			return true;
		};
	});
};
