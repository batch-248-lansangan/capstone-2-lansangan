/*const express = require("express");
const session = require('express-session');
const mongoose = require("mongoose");

//allows our backend to be available to our frontend application

const cors = require("cors");

//allows access to routes defined within our application
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");
const cartRoutes = require("./routes/cartRoutes");

const app = express();

// Set up session middleware
app.use(session({
  secret: 'your-secret-key',
  resave: false,
  saveUninitialized: false
}));

//allows all resources to access our backend application
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//Database Connection
	//Connect to MongoDB Atlas

mongoose.set('strictQuery',true);

mongoose.connect("mongodb+srv://admin:admin123@clusterbatch248.5padyji.mongodb.net/capstone-2?retryWrites=true&w=majority",
	{

	useNewUrlParser: true,
	useUnifiedTopology: true

	}
);

//connection to database
let db = mongoose.connection;
db.on("error",console.error.bind(console,"connection error"));
db.once("open",()=>console.log("Hi! We are connected to MongoDB Atlas!"))

//defines the /users string to be included for all user routes defined in the userRoutes.js
app.use("/users",userRoutes);
app.use("/products",productRoutes);
app.use("/orders",orderRoutes);
app.use("/carts", cartRoutes);

app.listen(process.env.PORT || 4000, ()=>{

	console.log(`API is now online on port ${process.env.PORT || 4000}`)

})*/

const express = require("express");
const session = require('express-session');
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const orderRoutes = require("./routes/orderRoutes");
const cartRoutes = require("./routes/cartRoutes");

const app = express();

app.use(session({
  secret: 'your-secret-key',
  resave: false,
  saveUninitialized: false
}));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

mongoose.set('strictQuery',true);

mongoose.connect("mongodb+srv://admin:admin123@clusterbatch248.5padyji.mongodb.net/capstone-2?retryWrites=true&w=majority", {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console,"connection error"));
db.once("open", ()=>console.log("Hi! We are connected to MongoDB Atlas!"));

app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);
app.use("/carts", cartRoutes);

const PORT = process.env.PORT || 4000;

app.listen(PORT, ()=> {
  console.log(`API is now online on port ${PORT}`);
});



