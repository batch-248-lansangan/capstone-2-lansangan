const mongoose = require("mongoose");

const cartSchema = new mongoose.Schema({

	name: {
		type: String
		//required: [true, "Product is required!"]
	},
	description: {
		type: String
		//required: [true, "Description is required!"]
	},
	price: {
		type: Number
		//required: [true, "Price is required!"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		//"new Date()" expressions instantiates a new "date" that stores the current date and time whenever course is created in our database
		default: new Date()

	},
	orders: [

		{
			orderId: {
				type: String
				//required: [true, "OrderId is required!"]
			},
			quantity: {
				type: Number
				//default: new Date()
			}


		}


	]


})

/*class Cart {
  constructor(existingCart = {}) {
    this.items = existingCart.items || {};
    this.totalQty = existingCart.totalQty || 0;
    this.totalPrice = existingCart.totalPrice || 0;
  }

	add(product, productId, qty) {
  let storedItem = this.items[productId];
  if (!storedItem) {
    storedItem = { product: product, qty: 0, price: 0 };
    this.items[productId] = storedItem;
  }

  storedItem.qty += qty;
  storedItem.price = storedItem.product.price * storedItem.qty;
  this.totalQty += qty;
  this.totalPrice += storedItem.product.price * qty;
	}

	changeQuantity(product, productId, qty) {
  let storedItem = this.items[productId];
  if (storedItem) {
    const productPrice = storedItem.product.price;
    this.totalQty = this.totalQty - storedItem.qty + qty;
    storedItem.qty = qty;
    storedItem.price = productPrice * qty;
    this.totalPrice = Object.values(this.items).reduce((acc, cur) => acc + cur.price, 0);
  	}
	}
}*/

class Cart {
  constructor(existingCart = {}) {
    this.items = existingCart.items || {};
    this.totalQty = existingCart.totalQty || 0;
    this.totalPrice = existingCart.totalPrice || 0;
  }

  add(product, productId, qty) {
    let storedItem = this.items[productId];
    if (!storedItem) {
      storedItem = { product: product, qty: 0, price: 0 };
      this.items[productId] = storedItem;
    }

    storedItem.qty += qty;
    storedItem.price = storedItem.product.price * storedItem.qty;
    this.totalQty += qty;
    this.totalPrice += storedItem.product.price * qty;
  }

  changeQuantity(product, productId, qty) {
    let storedItem = this.items[productId];
    if (storedItem) {
      const productPrice = storedItem.product.price;
      this.totalQty = this.totalQty - storedItem.qty + qty;
      storedItem.qty = qty;
      storedItem.price = productPrice * qty;
      this.totalPrice = Object.values(this.items).reduce((acc, cur) => acc + cur.price, 0);
    }
  }

  getItems() {
    return this.items;
  }
}


module.exports = Cart;


