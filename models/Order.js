const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({

	userId: {
		type: String
		//required: [true, "Product is required!"]
	},
	totalAmount: {
		type: Number
		//required: [true, "Price is required!"]
	},
	purchasedOn: {
		type: Date,
		//"new Date()" expressions instantiates a new "date" that stores the current date and time whenever course is created in our database
		default: new Date()

	},
	products: [

		{
			productId: {
				type: String
				//required: [true, "OrderId is required!"]
			},
			quantity: {
				type: Number
				//default: new Date()
			}

		}

	]


});

orderSchema.statics.getOrdersByUserId = function(userId) {
  return this.find({ user: userId });
};


const Order = mongoose.model('Order', orderSchema);

module.exports = Order;
//module.exports = mongoose.model("Order", orderSchema);
