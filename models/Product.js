const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({

	name: {
		type: String
		//required: [true, "Product is required!"]
	},
	description: {
		type: String
		//required: [true, "Description is required!"]
	},
	price: {
		type: Number
		//required: [true, "Price is required!"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		//"new Date()" expressions instantiates a new "date" that stores the current date and time whenever course is created in our database
		default: new Date()

	},
	orders: [

		{
			orderId: {
				type: String
				//required: [true, "OrderId is required!"]
			},
			quantity: {
				type: Number
				//default: new Date()
			}


		}


	]


})

module.exports = mongoose.model("Product", productSchema);