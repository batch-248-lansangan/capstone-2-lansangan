const express = require('express');
const router = express.Router();
const auth = require("../auth");
const Cart = require('../models/Cart');
const Product = require('../models/Product');
const orderController = require("../controllers/orderControllers");
const productController = require("../controllers/productControllers");
const cartController = require("../controllers/cartControllers");

// Import the app variable from index.js
const app = require('../index');



// Get current items in cart
router.get('/items', auth.verify, cartController.getCartItems);

//Add to cart
router.post('/add-to-cart', auth.verify, cartController.addToCart);

//Change quantity
router.put('/change-quantity/:productId', auth.verify, cartController.changeQuantity);


module.exports = router;