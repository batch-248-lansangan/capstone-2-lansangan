const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderControllers");
const productController = require("../controllers/productControllers");
const auth = require("../auth");
const Order = require("../models/Order");
const Product = require("../models/Product");


// POST /orders
router.post('/', async (req, res) => {
  try {
    const { userId, productId, quantity } = req.body;

    // Check if product exists
    const product = await Product.findById(productId);
    if (!product || !product.isActive) {
      return res.status(404).json({ error: 'Product not found' });
    }

    // Calculate total price
    const priceTotal = product.price * quantity;

    // Create new order
    const order = await Order.create({ userId, productId, quantity, priceTotal });

    // Update product's orders array
    product.orders.push({ orderId: order._id, quantity });
    await product.save();

    // Send response
    res.status(201).json({ order });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Server error' });
  }
});

//Get user orders by ID
router.get('/user/:userId/orders', async (req, res) => {
  try {
    const orders = await Order.getOrdersByUserId(req.params.userId);
    res.json(orders);
  } catch (error) {
    console.error(error.message);
    res.status(500).send('Server error');
  }
});


//Get all orders

router.get("/all",auth.verify,(req,res)=>{

  orderController.getAllOrders(req.params).then(resultFromController=>res.send(resultFromController));
});

module.exports = router;