const express = require("express");
const router = express.Router();
const productController = require("../controllers/productControllers");
const auth = require("../auth");


//Route for creating a product

router.post("/",auth.verify,(req,res)=>{


		//data.course.name
		//data.isAdmin	

	const data = {

		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin

	}

	productController.addProduct(data).then(resultFromController=>res.send(resultFromController))

});


//Route for retrieving all products

router.get("/all",(req,res)=>{

	productController.getAllProducts().then(resultFromController=>res.send(resultFromController));
});


//Route for retrieving all the ACTIVE products

router.get("/",(req,res)=>{

	productController.getAllActive().then(resultFromController=>res.send(resultFromController));
});

//Route for retrieving product by ID
router.get("/:productId",(req,res)=>{

    const productId = req.params.productId;
    productController.getProductById(productId).then(resultFromController=>{
        res.send(resultFromController);
    }).catch(err => {
        res.status(404).send({message: `Product with ID ${productId} not found`});
    });

});

//Update a product by ID
/*router.put("/:productId", auth.verify, (req, res) => {
    const productId = req.params.productId;
    const updatedProduct = {
        name: req.body.name,
        description: req.body.description,
        price: req.body.price
    };
    productController.updateProductById(productId, updatedProduct).then(resultFromController => {
        res.send(resultFromController);
    }).catch(err => {
        res.status(404).send({message: `Product with ID ${productId} not found`});
    });
});*/

router.put("/:productId", auth.verify, (req,res) => {

    const data = {
        product: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    };

    productController.updateProduct(data, req.params).then(resultFromController => res.send(resultFromController));
});



// Archive a product
/*router.patch("/:productId/archive", auth.verify, (req, res) => {
    const productId = req.params.productId;
    productController.archiveProduct(productId).then(resultFromController => {
        res.send(resultFromController);
    }).catch(err => {
        res.status(404).send({message: `Product with ID ${productId} not found`});
    });
});*/

router.put("/archive/:productId", auth.verify, (req, res) => {
  const data = {
    isAdmin: auth.decode(req.headers.authorization).isAdmin
  };
  productController.archiveProduct(data, req.params, req.body).then(resultFromController => res.send(resultFromController));
});

// Unarchive a product
/*router.patch("/:productId/unarchive", auth.verify, (req, res) => {
    const productId = req.params.productId;
    productController.unarchiveProduct(productId).then(resultFromController => {
        res.send(resultFromController);
    }).catch(err => {
        res.status(404).send({message: `Product with ID ${productId} not found`});
    });
});
*/

router.put("/unarchive/:productId", auth.verify, (req,res)=>{
    const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    };
    productController.unarchiveProduct(data, req.params, req.body).then(resultFromController => res.send(resultFromController));
});

//allows us to export the "router" object that will be accessed in our "index.js"
module.exports = router;