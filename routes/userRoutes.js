const express = require("express");
const router = express.Router();

const userController = require("../controllers/userControllers");
const orderController = require("../controllers/orderControllers");
const Product = require("../models/Product");
const Order = require("../models/Order");

const auth = require("../auth")


//Route for user registration
router.post("/register",(req,res)=>{

	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController))

});

//Route for user authentication

router.post("/login",(req,res)=>{

	userController.loginUser(req.body).then(resultFromController=>res.send(resultFromController));
});


//Route for check email

router.post("/checkEmail",(req,res)=>{
	userController.checkEmailExists(req.body).then(resultFromController=>res.send(resultFromController));
})


//Set user as an admin

router.put("/updateAdmin/:userId", auth.verify, (req,res)=>{

	userController.setAsAdmin(req.params, req.body).then(resultFromController=>res.send(resultFromController));
});


//Get user details by user ID

/*router.get("/details",auth.verify,(req,res)=>{

	const userData = auth.decode(req.headers.authorization);

	userController.getProfile({userId:userData.id}).then(resultFromController=>res.send(resultFromController));

})*/
router.get('/details', auth.verify, (req, res) => {
const userData = auth.decode(req.headers.authorization);
userController.getProfile({ userId: userData.id }).then((resultFromController) => res.send(resultFromController));
});

//purchase a product
router.post("/purchase/:productId", auth.verify, async (req, res) => {
const { productId } = req.params;
const user = req.user;

try {
// Find the product that the user wants to purchase
const product = await Product.findById(productId);
if (!product) {
return res.status(404).send({ message: "Product not found." });
}

    // Create a new order with the user ID and product ID
    const order = await Order.create({
      productId: productId,
      //userId: user.id,
      quantity: 1,
    });

    // Save the new order
    await order.save();

    // Send a response to the client confirming the purchase
    return res.send({ message: "Thank you for your purchase!" });
  } catch (error) {
    console.log(error);
    return res.status(500).send({ message: "Internal server error." });
  }
});


// [UPDATE USER DATA: START]

router.patch("/:userId", auth.verify, (req, res) => {

	userController.updateDetails(req.params, req.body).then(resultFromController => res.send(resultFromController));
});


module.exports = router;
